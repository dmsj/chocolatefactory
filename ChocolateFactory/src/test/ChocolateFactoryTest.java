package test;

import mediapark.chocolate.Charlie;
import mediapark.chocolate.ChocolateFactory;
import mediapark.test.AbstractChocolateTest;

import com.avaitkus.jobtask.chocolatefactory.CharlieChoice;
import com.avaitkus.jobtask.chocolatefactory.ChocolateFactoryManager;

public class ChocolateFactoryTest extends AbstractChocolateTest {

	private Charlie _charlie;
	private ChocolateFactory _chocolateFactor;
	@Override
	protected Charlie getCharlie() {
		if (_charlie == null) {
			_charlie = new CharlieChoice();
		}
		return _charlie;
	}

	@Override
	protected ChocolateFactory getChocolateFactory() {
		if (_chocolateFactor == null) {
			_chocolateFactor = new ChocolateFactoryManager();
		}
		_chocolateFactor.receiveCocoaShippment(1.0);
		return _chocolateFactor;
	}

}
