package com.avaitkus.jobtask.chocolatefactory;

import mediapark.chocolate.Charlie;
import mediapark.chocolate.ChocolateBar;

public class CharlieChoice extends Charlie {

	@Override
	protected boolean shouldEatChocolate(ChocolateBar chocolateBar) {
		if (getBloodSugarContentInKilograms() > 0) {
			return false;
		}
		if (chocolateBar.getSizeInKilograms() > 0.5
				|| chocolateBar.getSugarContentInPercent() > 5) {
			return false;
		}
		return true;
	}

	@Override
	public ChocolateBar showChocolateBarToAugustus(ChocolateBar chocolateBar,
			boolean fakeFriend) {

		if (fakeFriend) {
			return new FakeChocolateBar(chocolateBar);
		}
		return chocolateBar;
	}

}
