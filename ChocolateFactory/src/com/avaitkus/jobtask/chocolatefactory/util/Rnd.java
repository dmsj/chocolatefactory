package com.avaitkus.jobtask.chocolatefactory.util;

import java.util.Random;

public class Rnd {

	private static Random _randomInstance;

	public static double getDouble(double min, double max) {
		return min + (max - min) * getRandom().nextDouble();
	}

	private static Random getRandom() {
		if (_randomInstance == null) {
			_randomInstance = new Random();
		}
		return _randomInstance;
	}
}
