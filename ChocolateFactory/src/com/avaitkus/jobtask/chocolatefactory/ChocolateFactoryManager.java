package com.avaitkus.jobtask.chocolatefactory;

import java.math.BigDecimal;

import com.avaitkus.jobtask.chocolatefactory.util.Rnd;

import mediapark.chocolate.ChocolateFactory;
import mediapark.chocolate.DefaultChocolateBar;
import mediapark.chocolate.NoMoreCocoaException;

public class ChocolateFactoryManager implements ChocolateFactory {

	public static final double MAX_SIZE_IN_KILOGRAMS = 0.5;
	public static final double MIN_SIZE_IN_KILOGRAMS = 0.3;
	public static final double MAX_SUGAR_CONTENT_IN_PERCENT = 4.5;
	public static final double MIN_SUGAR_CONTENT_IN_PERCENT = 5.7;
	private double _cocoaInKilograms;

	@Override
	public DefaultChocolateBar produceNextChocolateBar()
			throws NoMoreCocoaException {

		_cocoaInKilograms -= 0.025;
		// because of floating point numbers are not exact
		// round _cocoaInKilograms value before check
		double cocoaLeft = new BigDecimal(_cocoaInKilograms).setScale(5,
				BigDecimal.ROUND_HALF_UP).doubleValue();
		if (cocoaLeft < 0) {
			throw new NoMoreCocoaException(
					"Not enough cocoa for chocolate bar.");
		}
		return new DefaultChocolateBar(Rnd.getDouble(MIN_SIZE_IN_KILOGRAMS,
				MAX_SIZE_IN_KILOGRAMS), Rnd.getDouble(
				MIN_SUGAR_CONTENT_IN_PERCENT, MAX_SUGAR_CONTENT_IN_PERCENT));
	}

	@Override
	public void receiveCocoaShippment(double cocoaInKilogram) {
		_cocoaInKilograms += cocoaInKilogram;
	}

}
