package com.avaitkus.jobtask.chocolatefactory;

import mediapark.chocolate.ChocolateBar;

public class FakeChocolateBar implements ChocolateBar {

	private final ChocolateBar _realChocolateBar;

	public FakeChocolateBar(ChocolateBar chocolateBar) {
		_realChocolateBar = chocolateBar;
	}

	@Override
	public double getSizeInKilograms() {
		return _realChocolateBar.getSizeInKilograms();
	}

	@Override
	public double getSugarContentInPercent() {
		return 0;
	}

	@Override
	public void takeABite(double biteInKilograms) {
		_realChocolateBar.takeABite(biteInKilograms);
	}

}
